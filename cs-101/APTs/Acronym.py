'''
Created on Feb 5, 2017

@author: rdn5
'''
def acronym(phrase): 
    # this function takes a phrase and returns the acronym
    phraseList = phrase.split()
    for i in phraseList:
        if i[0].isupper():
            return ''.join([i[0] for i in phraseList]).upper()
        else:
            pass
print acronym("Ryan Daniel Nicholson")
if __name__ == '__main__':
    pass