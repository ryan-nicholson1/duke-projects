'''
Created on Mar 8, 2017

@author: rdn5
'''
def countall(words):
    count = 0
    i = 0
    while i < len(words)-1:
        if words[i][0].lower() == words[i+1][0].lower():
            count +=1
            i+=2
        else:
            i+=1
    return count

print countall(["He", "has", "four", "fanatic", "fantastic", "fans"])
print countall(["There", "may", "be", "no", "alliteration", "in", "a", "sequence"])