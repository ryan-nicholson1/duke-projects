'''
Created on Mar 7, 2017

@author: rdn5
'''
def getMaximumSubset(words):
    return len(set([''.join(sorted(w)) for w in words]))
 
 
print getMaximumSubset(["creation","sentence","reaction","sneak","star","rats","snake"])