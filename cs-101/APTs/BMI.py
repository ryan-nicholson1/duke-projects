'''
Created on Jan 26, 2017

@author: rdn5
'''

def calculate(weight,height):
      """
      return float indicating BMI
      (body mass index)
      given weight in pounds (float)
      given height in inches (float)
      """
      height = float(height)
      weight = float(weight)
      BMI = 703.0695 * weight/(height ** 2)
      print BMI
      return float(BMI)
calculate(168.0, 69)