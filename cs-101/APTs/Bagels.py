'''
Created on Feb 10, 2017

@author: rdn5
'''
def bagelCount(orders):
    total = 0
    for order in orders:
        if order < 12:
            total= total + order
        if order >= 12:
            total = total + order + int(order/12)
    return total