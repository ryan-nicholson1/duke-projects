import random

def loadWords(filename):
    """
    read words from specified file and return a list of
    strings, each string is one line of the file
    """
    allwords = []
    f = open(filename)
    for line in f:
        line = line.strip()
        allwords.append(line)
    f.close()
    return allwords
    
     
def getWords(allwords,wordlength):
    """
    returns a list of words having a specified length from
    allwords
    """
    wlist = [w for w in allwords if len(w) == wordlength]
    return wlist

def display(guess):
    '''
    create a string from list guess to print/show user
    '''
    return ' '.join(guess)

def makeSecretList(secret):
    '''
    Create the list that's modificable to track letters 
    guessed by user
    '''
    return ['_']*len(secret)

def doGame(word):
    guess = makeSecretList(word)
    misses = 8 #Number of allowed misses until user loses the game
    alpha = "abcdefghijklmnopqrstuvwxyz"
    guessed = list(alpha) #List of letters in alphabet to be modified when letter is guessed to show player which letters are still available
    while True:
        if misses == 0: #User has guessed too many incorrect letters
            break
        print
        print "number of misses left:",misses #To show user how many misses are left until game is over
        print "Letters not yet guessed:",''.join(guessed) #Show user the letters that have not been guessed
        if guess.count('_') == 0: #Word has been guessed
            break
        print "secret so far:",display(guess) #Coded word with underscores for letters; modified if letter is guessed
        letter = raw_input("guess a letter: ")
        for index in range(len(word)):
            if word[index].lower() == letter.lower():
                guess[index] = word[index]
        if letter.lower() not in word and letter.lower() in guessed: #To modify the misses left until game is over
            misses += -1
        if letter.lower() in guessed: #To modify the string of letters not guessed displayed to the user
            dex = guessed.index(letter.lower())
            guessed[dex] = " "
    if guess.count("_") == 0:
        print "word is guessed!",word
    else:
        print "you lost! word is",word
 
def play(allwords):
    wlen = int(raw_input("how many letters in word you'll guess? "))
    words = getWords(allwords,wlen)    
    word = random.choice(words)
    doGame(word)

if __name__ == '__main__':
    allwords = loadWords("lowerwords.txt")
    play(allwords)