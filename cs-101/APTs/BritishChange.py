'''
Created on Feb 10, 2017

@author: rdn5
'''
def coins(pence):
    pounds = pence/240
    poundleftover=pence%240
    shillings = poundleftover/12
    shillingleftover = poundleftover%12
    pennies = shillingleftover
     
    return [pounds,shillings,pennies]