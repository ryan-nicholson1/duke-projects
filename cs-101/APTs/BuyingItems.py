'''
Created on Apr 3, 2017

@author: rdn5
'''
def costItems(have, prices):
    '''
    Given two string parameters
    have and prices, where have
    is a string of items one already has, 
    and prices is a string of items and 
    their prices that one needs to get.
    Note that both have and prices could 
    have duplicate items.
    This function returns the cost of 
    purchasing one of every item you need
    and do not already have. 
    '''
    possessions = have.split(",")
    pricesList = prices.split(",")
    alist = []
    blist = []
    newPrice = 0
    for x in range(len(pricesList)):
        if x % 2 == 0:
            alist.append(pricesList[x])
        else:
            blist.append(int(pricesList[x]))
    for i in range(len(alist)):
        if alist[i] not in possessions:
            newPrice += blist[i]
            possessions.append(alist[i])
    return newPrice
print costItems("apple,fig,banana,fig,apple","apple,2,carrot,1,broccoli,3,apple,2,carrot,1,broccoli,3")