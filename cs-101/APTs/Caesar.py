'''
Created on Feb 10, 2017

@author: rdn5
'''
def decode(cipher, shift):
    alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
     
    message = ""
     
    for x in cipher:
        indx = alpha.find(x)
        message += alpha[indx-shift]
         
    return message