'''
Created on Apr 4, 2017

@author: rdn5
'''
def nameDonor(contributions):
    aDict = {}
    for line in contributions:
        (name,price) = line.split(":")
        if name not in aDict:
            aDict[name] = float(price)
        else:
            aDict[name] += float(price)
    highest = max(aDict.values())
    maxList = [k for k, v in aDict.items() if v == highest]
    maxList = sorted(maxList)
    if len(maxList) != 0:
        answer = maxList[0]
    return answer
print nameDonor(['Smith:100.00', 'Smith:2000.00', 'Williams:3000.00', 'Jackson:1500.00', 'Jackson:2300.00', 'Smith:400.00', 'Smith:250.00', 'Best:3800.00'])