'''
Created on Mar 8, 2017

@author: rdn5
'''
def uniqueLetters(string):
    newstring = list(string)
    answer = ""
    for let in newstring:
        if let in answer:
            return False
        elif let in string:
            answer = answer + let
            result = True
    return True

def maxLength(letters):
    newletters = list(letters)
    answer = []
    newanswer = []
    pos = 0
    for let in newletters:
        for i in range(len(letters)):
            new = letters[pos:i+1]
            if uniqueLetters(new):
                count = len(new)
                answer.append(count)
        total = answer[-1]
        newanswer.append(total)
        pos += 1
    return max(newanswer)

if __name__ == '__main__':
    print maxLength("srm")
    print maxLength("dengklek")
    print maxLength("haha")
    print maxLength("www")
    print maxLength("thisisanaptbeforethedukemidterm")
