'''
Created on Feb 5, 2017

@author: rdn5
'''
def acronym(phrase): 
    # this function takes a phrase and returns the acronym
    phraseList = phrase.split()
    for i in phraseList:
            return str(''.join([i[0] for i in phraseList]))
if __name__ == '__main__':
    pass