'''
Created on Apr 3, 2017

@author: rdn5
'''
def reportDuplicates(names):
    alist = []
    aDict = {}
    for name in names:
        if name not in aDict:
            aDict[name] = 1
        else:
            aDict[name] += 1
    info = aDict.items()
    for i in info:
        if i[1] > 1:
            alist.append(i[0] + " " + str(i[1]))
    return sorted(alist)
print reportDuplicates(["JOHN", "BOB", "JOHN", "BILL", "STANLEY", "JOHN"])
if __name__ == '__main__':
    pass