'''
Created on Mar 23, 2017

@author: rdn5
'''

def reversecomp(dna):
    #('t' gets replaced with 'a', 'a' with 't', 'c' with 'g' and 'g' with 'c'
    comp = ""
    for let in dna:
        if let == "t":
            comp += "a"
        if let == "a":
            comp += "t"
        if let == "c":
            comp += "g"
        if let == "g":
            comp += "c"
    reverseDNA = []
    for char in range(len(comp)-1, -1, -1):
        reverseDNA.append(comp[char])
    reverseDNAstr = "".join(reverseDNA)
    return reverseDNAstr