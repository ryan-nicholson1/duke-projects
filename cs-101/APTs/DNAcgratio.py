'''
Created on Feb 2, 2017

@author: rdn5
'''
def ratio(dna):
     
    dna_length = len(dna)
     
    dna_length_float = float(dna_length)
     
    cg_total = dna.count('c') + dna.count('g')
     
    return cg_total/dna_length_float
