'''
Created on Apr 25, 2017

@author: rdn5
'''
import operator

def winners(data):
    d = {}
    for n in data:
        s = n.split()
        name = s[0]
        time = s[1].split(":")
        secs = int(time[0])*60+int(time[1])
        if name not in d:
            d[name] = []
        d[name].append(secs)
    tups = [(x, len(d[x]), sum(d[x])) for x in d]
    sorted_tups = sorted(tups, key=operator.itemgetter(2))
    last = sorted(sorted_tups, key=operator.itemgetter(1), reverse=True)
    return [e[0] for e in last]

if __name__ == '__main__':
    pass