'''
Created on Mar 7, 2017

@author: rdn5
'''
from encore.storage.tests.static_url_store_test import count

def howMany(meals, restaurant):
    """
    This function has two parameters. The first, meals,
    is a list of strings with each in the format
    "name:restaurant" where name is the name of a 
    person, and restaurant the name of a restaurant the
    person has eaten a meal at. The second parameter is 
    restaurant, a string that is the name of a 
    restaurant. This function returns the number of 
    unique people who have eaten at the restaurant.
    """
    alist = []
    blist = []
    for line in meals:
        name = line.split(":")[0]
        rest = line.split(":")[1]
        alist.append(name)
        blist.append(rest)
    count = 0
    answer = []
    for idx in range(len(blist)):
        if restaurant == blist[idx]:
            if alist[idx] not in answer:
                answer.append(alist[idx])
                count += 1
            else:
                pass
        else:
            pass
    return count

print howMany(["Sue:Elmos", "Sue:Elmos", "Sue:Elmos"], "Elmos")
        
        
        
        