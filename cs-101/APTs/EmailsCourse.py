'''
Created on Apr 3, 2017

@author: rdn5
'''
import collections
def emailsLargest(courses):
    aDict = {}
    for line in courses:
        alist = line.split(":")
        if alist[0] not in aDict:
            aDict[alist[0]] = [alist[2]]
        else:
            aDict[alist[0]].append(alist[2])
    d = collections.OrderedDict(sorted(aDict.items()))
    alist = []
    for k, v in d.items():
        alist.append((k, v))
    highest = max(alist)
    maxList = [k for k, v in aDict.items() if v == highest]
    print maxList
    return alist
    
print emailsLargest(['A1:M:m@duke.edu', 'F1:GH:gh@duke.edu', 'W1:SH:sh@duke.edu', 'W1:D:d@duke.edu', 'C1:GH:gh@duke.edu', 'A1:SH:sh@duke.edu'])