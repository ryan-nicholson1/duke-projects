'''
Created on Jan 26, 2017

@author: rdn5
'''

def calculate(foo, bar):
    '''
    Given the two integer parameters, foo and bar, 
    return the strength of a fizzle using the formula given
    '''
    foo = float(foo)
    bar = float(bar)
    strength = 3 +  (bar ** 2) / (foo - 1)
    print strength
    return float(strength)
