'''
Created on Feb 2, 2017

@author: Ryan Nicholson
@netID: rdn5
Gravity APT
'''
def falling(time,velo):
    # this function takes parameters time and
    # velo to compute how far a ball travels
    # to drop from an infinitely tall building
    v = velo
    t = time
    g = 9.8
    d = v * t + 0.5 * g * t ** 2
    d = float(d)
    return d

