'''
Created on Jan 26, 2017

@author: rdn5
'''

def convert(r,g,b):
      """
      return float value obtained by
      converting integer r,g,b, into grayscale
      """
      
      grayscale = 0.21*r + 0.71*g + 0.07*b
      print grayscale
      return float(grayscale)