'''
Created on Mar 28, 2017

@author: rdn5
'''

def combine(foreground, background):
    alist = []
    count = 0
    for i in foreground:
        r,g,b = tuple(i.split(","))
        if int(g) > int(r) + int(b):
            alist.append(background[count])
        else:
            alist.append(foreground[count])
        count += 1
    return alist
            