'''
Created on Apr 3, 2017

@author: rdn5
'''
def numWords(word, phrase):
    '''
    Given string parameters word and phrase, 
    where word is one word and phrase is a
    string of words separated by a blank, return 
    the count of the unique words in phrase 
    that come after word in alphabetical order
    '''
    aList = []
    wordList = phrase.split()
    word = word.lower()
    for i in wordList:
        i = i.lower()
        if i == word:
            pass
        else:
            if sorted([word,i])[0] == word:
                aList.append(i)
    aList = set(aList)
    print aList
    return len(aList)
print numWords("a", "This is a very long long long test of this phrase")
