'''
Created on Feb 2, 2017

@author: Ryan Nicholson
@netID: rdn5
'''
from __builtin__ import int
def minutesNeeded(m): 
    # this function returns how many minutes
    # it takes to do m loads of laundry
    # m is an integer value
    time = (m * 25) + 25 + 10
    return time
