'''
Created on Apr 25, 2017

@author: rdn5
'''
import operator
 
def generate(results):
    countryList = []
    for tourn in results:
        countries = tourn.split()
        countryList.extend(countries)
    print countryList
    countrySet = list(set(countryList))
    countryDict = {}
    for country in countrySet:
        countryDict[country] = [0,0,0]
    print countryDict
    i = 1
    for medal in countryList:
        if i % 3 == 1:
            countryDict[medal][0] += 1
        elif i % 3 == 2:
            countryDict[medal][1] += 1
        elif i % 3 == 0:
            countryDict[medal][2] += 1
        i+=1  
    print countryDict
     
    sorted_x = sorted(countryDict.iteritems(), key=operator.itemgetter(0))
    sorted_x = sorted(sorted_x, key=operator.itemgetter(1), reverse=True)
    print sorted_x
     
    answerList = []
    for entry in sorted_x:
        newEntry = "" + entry[0] + " " + str(entry[1][0]) + " " + str(entry[1][1]) + " " + str(entry[1][2]) + ""
        answerList.append(newEntry)
    return answerList

if __name__ == '__main__':
    print generate(["ITA JPN AUS", "KOR TPE UKR", "KOR KOR GBR", "KOR CHN TPE"])