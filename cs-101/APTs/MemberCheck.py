'''
Created on Apr 3, 2017

@author: rdn5
'''
def whosDishonest(club1, club2, club3):
    cheatI = [x for x in club1 if x in club2 or x in club3]
    cheatII = [x for x in club2 if x in club1 or x in club3]
    cheatIII = [x for x in club3 if x in club2 or x in club1]
    cheats = cheatI +cheatII+cheatIII
    return sorted(list(set(cheats)))
