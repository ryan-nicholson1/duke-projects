'''
Created on Feb 27, 2017

@author: rdn5
'''
def decrypt(library,message):
    """
    return String for parameters
    library a list of Strings
    and message a string
    """
    alist = []
    astring = ""
    for x in library:
        letterCode = x.split()
        letter = letterCode[0]
        morseLetter = letterCode[1]
        alist.append(morseLetter)
        alist.append(letter)
    messageList = message.split()
    for i in messageList:
        if i in alist:
            letterIDX = alist.index(i) + 1
            if (letterIDX) > len(alist):
                pass
            else:
                astring += alist[letterIDX]
        else:
            astring += "?"
    return astring
print decrypt(["O ---", "S ..."],"... --- ...")