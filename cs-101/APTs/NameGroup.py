'''
Created on Feb 23, 2017

@author: rdn5
'''

def calculate(names, one, two, space):
    alist = []
    astring = ""
    if one in names and two in names:
        nameslist = names.split()
        oneIDX = nameslist.index(one)
        namesLength = len(nameslist) - 1
        xcount = oneIDX
        while True:
            if (xcount + space + 1) > namesLength:
                break
            elif nameslist[xcount] == one and nameslist[xcount + space + 1] == two:
                astring = " ".join(nameslist[xcount: (xcount + space + 2)])
                if astring.count(one) > 1 or astring.count(two) > 1:
                    astring = ""
                    xcount += 1
                else:
                    break
            else:
                xcount += 1
        if astring.count(one) > 1 or astring.count(two) > 1:
            astring = ""
        else:
            pass
    return astring

    
print calculate("a b c d b f g a b c d e f g a b c", "b", "g", 4)
            