'''
Created on Feb 2, 2017

@author: rdn5
'''
import math
 
def minutesNeeded (numCakes, capacity):
 
    numCakes = float(numCakes)
 
    capacity = float(capacity)
 
    if numCakes <= 0:
 
        total_time = 0
 
    elif numCakes < capacity:
 
        total_time = 10
 
    else:
 
        TotalSides = (numCakes * 2.0)/capacity
 
        total_time = math.ceil(TotalSides) * 5.0
 
        total_time = int(total_time)
 
    return total_time