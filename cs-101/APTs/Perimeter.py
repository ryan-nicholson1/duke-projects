'''
Created on Jan 26, 2017

@author: rdn5
'''

def total(width,length):
      """
      return float indicating 
      the perimeter of the rectangle that 
      is specified by the float parameters 
      width and length. All measurements are in inches.
      """
      perimeter = (2 * width) + (2 * length)
      print perimeter
      return float(perimeter)

