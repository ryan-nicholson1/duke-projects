'''
Created on Feb 10, 2017

@author: rdn5
'''
def isvowel(ch):
    if "aeiou".count(ch) >= 1:
        return True
    else:
        return False
 
def isconsonant(ch):
    return not isvowel(ch)
 
def convert(word):
    if isvowel(word[0]):
        return word+"-way"
    if isconsonant(word[0]):
        if word[0]=="q":
            if not word[1]=="u":
                return word[1:len(word)]+"-quay"
            else:
                return word[2:len(word)]+"-quay"
        else:
            for t in range(1,len(word)):
                if isconsonant(word[t]):
                    pass
                else:
                    return word[t:len(word)]+"-"+word[0:t]+"ay"
                    break
            return word + "-way"
if __name__ == '__main__':
    pass