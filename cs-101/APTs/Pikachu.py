'''
Created on Feb 23, 2017

@author: rdn5
'''
def check(word):
    poss_words = ["pi","ka", "chu"]
    ok = True
    i = 0
    while i < len(word) - 1:
        if word[i] == "p":
            if word[i+1] == "i":
                ok = True
                i += 2
            else:
                return "NO"
                 
        elif word[i] == "k":
            if word[i+1] == "a":
                ok = True
                i += 2
            else:
                return "NO"
        elif word[i] == "c":
            if word[i+1:i+3] == "hu":
                ok = True
                i += 3
            else:
                return "NO"
        else:
            return "NO"
    return "YES"
 
print check ("pikapi")
print check ("pipikachu")
print check ("pikaqiu")
