'''
Created on Feb 7, 2017

@author: Ryan Nicholson
@netid: rdn5
'''
def compute(itema, itemb):
    '''
    Given the integer parameters itema
    and itemb, return a decimal number 
    that is the result when these numbers 
    are applied in the likelihood formula.   
    '''
    itema = float(itema)
    itemb = float(itemb)
    likelihood = (5* itema / 3) + ((itemb ** 2)/7 )
    return likelihood
print compute(3, 42)
if __name__ == '__main__':
    pass