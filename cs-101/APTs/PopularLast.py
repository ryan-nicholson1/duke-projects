'''
Created on Apr 3, 2017

@author: rdn5
'''
def lastNames(selection):
    '''
    Given the string parameter selection,
    which contains first and last names
    for one or more people, return the 
    string of sorted last names that 
    are more popular as a last name 
    then as a first name.
    '''
    nameDict = {}
    alist = selection.split(",")
    for name in alist:
        (first,last) = name.split()
        if first not in nameDict:
            nameDict[first] = [1,0]
        else:
            nameDict[first][0] += 1
        if last not in nameDict:
            nameDict[last] = [0,1]
        else:
            nameDict[last][1] += 1
    answer = []
    info = nameDict.items()
    for i in info:
        if i[1][0] < i[1][1]:
            answer.append(i[0])
    answerSorted = sorted(answer)
    return " ".join(answerSorted)
        
if __name__ == '__main__':
    pass