'''
Created on Feb 26, 2017

@author: rdn5
'''
import math

def isPrime(number):
    limit = int(math.sqrt(number))+1
    if number < 2:    # must be greater than 1
        return False
    if number < 4:    # must be 2 or 3
        return True
    if number%2 == 0:  # even
        return False
    for n in range(3,limit,2):   # just check odd numbers
        if number/n * n == number:
            return False
    return True

def pcount(low,high):
    """
    return int, the number of prime numbers
    between int values low and high, inclusive
    """
    alist = range(low, high + 1) 
    count = 0
    for i in alist:
        if isPrime(i):
            count += 1
        else:
            pass
    return count
