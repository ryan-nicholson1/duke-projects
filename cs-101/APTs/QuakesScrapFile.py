'''
Created on Feb 8, 2017

@author: rdn5
'''
import urllib2
def fileToList(url):
    '''
    This function reads a file from a given url 
    returns a list of strings where each string 
    represents one line from the file
    '''
    alist = []
    dollarSign = "$"
    source = urllib2.urlopen(url)
    for line in source:
        items = line.strip()
        place = items.split(dollarSign, 1)[0]
        magnitude = items.split(dollarSign, 1)[1]
        aline = (magnitude + ", " + place)
        alist.append(aline)
    return alist 
def printQuakes(lst, number):
    # this is the function that prints the earthquake full
    # information. If the number given is -1 or greater than
    # the entries in the list, the full list is returned. If 
    # the number is greater than or equal to 1, then element 
    # 0 to the given number is returned.
    if number == -1:
        return "\n".join(lst)
    elif number >= 1:
        return "\n".join(lst[0:number])
    elif number > list:
        return "\n".join(lst)
def largeQuakes(mag, lst):
    # this function takes two parameters: mag and list. It searches
    # the given list for earthquakes with magnitudes higher than the
    # given mag value and returns the full info.
    magnitudeList = []
    placeList = []
    finalList = ""
    for line in lst:
        i = line.strip()
        magnitude = i.split(", ", 1)[0]
        place = i.split(", ", 1)[1]
        aline = (magnitude + ", " + place)
        placeList.append(aline)
        magnitudeList.append(magnitude)
    magnitudeList = [float(i) for i in magnitudeList]
    list1 = [None]*(len(magnitudeList)+len(placeList))
    list1[::2] = magnitudeList
    list1[1::2] = placeList
    for i in range(0,len(list1)-1,2):
        if list1[i] >= mag:
            finalList = finalList + list1[i+1] + "\n"
        elif list1[i] < mag:
            pass
    return finalList
def locationQuakes(place, lst):
    # this function takes two parameter. It searches for 
    # the place string within the list. Then it returns
    # the full information of all earthquakes from that
    # place
    placeList = []
    for x in range(len(lst)):
        if place in lst[x]:
            placeList.append(lst[x])
        elif place not in lst[x]:
            pass
    return placeList
def maxMagnitude(lst):
    # this function takes a list and finds the maximum magnitude
    # from the list. Then returns the corresponding information
    # from where the earthquake took place.
    magnitudeList = []
    placeList = []
    for line in lst:
        i = line.strip()
        magnitude = i.split(", ", 1)[0]
        place = i.split(", ", 1)[1]
        aline = (magnitude + ", " + place)
        placeList.append(aline)
        magnitudeList.append(magnitude)
    magnitudeList = [float(i) for i in magnitudeList]
    list1 = [None]*(len(magnitudeList)+len(placeList))
    list1[::2] = magnitudeList
    list1[1::2] = placeList
    maxMag = list1.index(max(magnitudeList))+1
    return list1[maxMag]

if __name__ == '__main__':
    '''
    The earthquake program
    Read a file of earthquake data and answer
    several queries about the data.
    '''
    urlstart = "http://www.cs.duke.edu/courses/compsci101/spring17/data/"
    #datafile = "earthquakeDataJan29-2017.txt"
    datafile = "earthquakeDataSmall.txt"
    eqList = fileToList(urlstart+datafile)
    print "First six lines in the list:"
    print range(4)
