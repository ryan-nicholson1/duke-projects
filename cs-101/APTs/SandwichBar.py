'''
Created on Mar 2, 2017

@author: rdn5
'''
def whichOrder(available, orders):        # line 1
    for index in range(len(orders)):      # line 2
        ok = True                         # line 3
        for w in orders[index].split():    # line 4
            if not w in available:        # line 5
                ok = False
        if ok == True:
            return index
    return -1  
 
print whichOrder([ "cheese", "cheese", "cheese", "tomato" ],["ham ham ham", "water", "pork", "bread", "cheese tomato cheese", "beef"])