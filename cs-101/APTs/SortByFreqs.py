'''
Created on Apr 3, 2017

@author: rdn5
'''
import operator
         
def sort(data):
    dict={}
    for word in data:
        dict[word] = dict.get(word, 0) + 1
    resultList = sorted(dict.items(), key=operator.itemgetter(0))
    resultList = sorted(resultList, key=operator.itemgetter(1), reverse=True)
     
    resultList = [ name for (name, num) in resultList]
    return resultList        
 
         
print sort(["apple", "pear", "cherry", "apple", "cherry", "pear", "apple", "banana"])