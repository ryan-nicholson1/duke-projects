'''
Created on Mar 28, 2017

@author: rdn5
'''
def countVisible(trophies):
    visible_left = 0
    max_left = 0
    for trophy in trophies:
        if trophy > max_left:
            visible_left +=1
            max_left = trophy
     
    trophies_flip = []
    visible_right = 0
    max_right = 0
    for i in reversed(trophies):
        trophies_flip.append(i)
    for trophy in trophies_flip:
        if trophy > max_right:
            visible_right +=1
            max_right = trophy
    visible = []
    visible.append(visible_left)
    visible.append(visible_right)
    return visible