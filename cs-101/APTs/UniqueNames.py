'''
Created on Mar 23, 2017

@author: rdn5
'''
def namesForYear(courses, year):
    """
    courses is a list of strings, each string 
    represents a course in the format described

    year represents a year, could be any string
    return a string of the unique names 
    matching year, sorted, space-separated
    """
    alist = []
    for line in courses:
        lineSplit = line.split(":")
        for i in range(len(lineSplit)):
            if lineSplit[i] == year:
                alist.append(lineSplit[i-1])
    aNewList = list(set(alist))
    blist = sorted(aNewList)
    answer = " ".join(blist)
    return answer

if __name__ == '__main__':
    print namesForYear(["cps6:James:firstyear:Merlin:sophomore:Blake:senior:Yin:senior:Gauf:junior",
        "math31:Smith:firstyear:Maxwell:sophomore:Blake:senior:Yin:senior:Gauf:junior",
        "french2:Yin:firstyear:Gauf:sophomore:Knuth:senior:Lee:firstyear",
        "german1:Gauf:sophomore:Wesley:senior:Lee:firstyear:James:firstyear:Merlin:sophomore"], "firstyear")