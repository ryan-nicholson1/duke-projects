'''
Created on Mar 23, 2017

@author: rdn5
'''
def numberUnique(zoos):
    fullSet = set() #All the animals. No duplicates set(["zebra", "bear", "fox", "rhino", "kangaroo",...])
    allZoo = [] #[set(["zebra", "bear", "fox"]), set(["fox", "bear"]), ...]
     
    for line in zoos: #line = "zebra bear fox elephant" 2="bear crocodile fox"
        animalList = line.split() #wordList = ["zebra", "bear", "fox", "elephant"], ["bear", "croc", "fox"], ...etc
         
        print animalList
         
        tempSet = set()
        for animal in animalList: #animal = "zebra"
            fullSet.add(animal)
            tempSet.add(animal)
        allZoo.append(tempSet) #appending the completed zoo set ????????
     
#    print fullSet
#    print allZoo
 
 
    uniqueZoo = set()
        
    for animal in fullSet: #animal = "zebra"
        redBox = []
        for zoo in allZoo: #zoo = (["zebra", "bear", "fox"])
            if animal in zoo: #if zebra is in zoo
                redBox.append(allZoo.index(zoo))
        if len(redBox) == 1:
            uniqueZoo.add(redBox[0]) #uniqueZoo = ([0])
             
    return len(uniqueZoo)
if __name__ == '__main__':
    print numberUnique(["fox", "python", "goat", "bear", "zebra"] )
    print numberUnique(["zebra bear fox elephant", "bear crocodile fox", "rhino elephant crocodile kangaroo", "elephant bear"])
    print numberUnique(["a b c d e", "f g h i j k", "l m n o p", "c h n"] )