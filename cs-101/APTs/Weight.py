'''
Created on Feb 7, 2017

@author: rdn5
'''
def predict(one, two, three):   
    '''
    Given the three integer parameters, one, 
    two and three, return a decimal number 
    that is the average of the values 
    squared.
    '''
    one = float(one)
    two = float(two)
    three = float(three)
    mean = ((one + two + three) / 3) ** 2
    return mean
print predict(1, 2, 4)
if __name__ == '__main__':
    pass