'''
Created on Mar 23, 2017

@author: rdn5
'''
def score(listA,listB,listC):
    """
    return String based on three string list parameters
    """
    aScore = 0
    bScore = 0
    cScore = 0
    scoreList = []
    for word in listA:
        if word not in listB and word not in listC:
            aScore += 3
        if (word in listB and word not in listC) or (word in listC and word not in listB):
            aScore += 2
        if word in listB and word in listC:
            aScore += 1
    scoreList.append(str(aScore))
    for word in listB:
        if word not in listA and word not in listC:
            bScore += 3
        if (word in listA and word not in listC) or (word in listC and word not in listA):
            bScore += 2
        if word in listA and word in listC:
            bScore += 1
    scoreList.append(str(bScore))
    for word in listC:
        if word not in listA and word not in listB:
            cScore += 3
        if (word in listA and word not in listB) or (word in listB and word not in listA):
            cScore += 2
        if word in listA and word in listB:
            cScore += 1
    scoreList.append(str(cScore))
    return "/".join(scoreList)

print score(listA = ["cat", "dog", "pig", "mouse"],
listB = ["cat", "pig"],
listC = ["dog", "cat"])