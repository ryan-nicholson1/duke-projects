'''
Created on Apr 5, 2017

@author: rdn5
'''
import Tools, random, operator
 
DEBUG = True
 
def change_knowledge(guess,secret,knowledge):
    """ Changes the knowledge that is shown to the player
        For example changes  _ e _ _ e  to  t e _ _ e  when t is guessed.
    """
    for i,ch in enumerate(secret):
        if ch == guess and knowledge[i] == '_':
            knowledge[i] = ch
    return knowledge
 
def display(guesses,knowledge):
    """ This function print information to the human player. """
    print "you've guessed",guesses
    print "your knowledge so far"
    print ' '.join(knowledge)
    print
     
def process(knowledge, guess, partial_list):
    combos = {}
    for word in partial_list:
        key = []
        for letter in knowledge:
            key.append(letter)
        for i,letter in enumerate(word):
            if letter == guess:
                key[i] = letter
        key = "".join(key)
        if key not in combos:
            combos[key] = []
        combos[key].append(word)
    return combos
          
#print process(['_', '_', '_', '_', '_'],"g",["guyss","hello","ggggg","strin"])             
 
def play_game():
    global DEBUG
    sec_length = 5
    whole_list = Tools.get_words("lowerwords.txt",sec_length)
    partial_list = whole_list
    secret = random.choice(whole_list)
    knowledge = ["_"]*len(secret)
    guesses = []   # letters guessed so far
    misses = 0     # misses made so far
    misses_allowed = 12
     
    while misses < misses_allowed:
        display(guesses,knowledge)
         
        print "guesses left: ",misses_allowed-misses
        print "guess a letter: "
        if DEBUG: print "   (secret is", secret.upper(), ")"
        guess = raw_input()
        guesses.append(guess)
        print ""
        combos = process(knowledge, guess, partial_list)
        #print combos
        x = [(t,len(combos[t])) for t in combos.keys()]
        y = sorted(x, key=operator.itemgetter(1), reverse = True)
        print y
        partial_list = combos[y[0][0]]
        print len(partial_list)
        secret = random.choice(partial_list)
        if guess in secret:
            print "you guessed a letter"
            knowledge = change_knowledge(guess,secret,knowledge)
        else:
            print "boo-hoo, no, sorry"
            misses = misses + 1
        
         
        if not "_" in knowledge:
            print "you win!"
            break
    print "game over, you win! secret word was ",secret
 
if __name__ == '__main__':
    play_game()