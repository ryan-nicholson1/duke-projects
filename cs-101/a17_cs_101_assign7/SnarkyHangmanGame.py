'''
Created on Apr 5, 2017

@author: rdn5
'''
import random, operator
 
DEBUG = False #sets the global variable DEBUG to false

def load_lines(filename):
    """
    read words from specified file and return a list of
    strings, each string is one line of the file
    """
    lines = []
    f = open(filename)
    for line in f.readlines():
        line = line.strip()
        lines.append(line)
    return lines
       
def getPossibleWords(filename, wordlength):
    """
    returns a list of words having a specified length from
    the file whose name is filename.
    """
    lines = load_lines(filename)
    wlist = [w for w in lines if len(w) == wordlength]
    return wlist
 
def changeInfo(guess,secret,info):
    """ Changes the info that is shown to the player
        For example changes  _ e _ _ e  to  t e _ _ e  when t is guessed.
    """
    for i,ch in enumerate(secret):
        if ch == guess and info[i] == '_':
            info[i] = ch
    return info
 
def display(guesses,info):
    """ This function print information to the human player. """
    print "letters missed: "," ".join(guesses)
    print "Progress:"
    print ' '.join(info)
    print
     
def process(info, guess, partialList): 
    '''
    This function takes a list (partialList) and a character given by the user (guess),
    and the string of previous correct guesses/_ (info). It returns the dictionary that
    contains all the categorized possible letter strings with the number of matching words
    in combos in tuple form.
    '''
    combos = {}
    for word in partialList:
        key = []
        for letter in info:
            key.append(letter)
        for i,letter in enumerate(word):
            if letter == guess:
                key[i] = letter
        key = "".join(key)
        if key not in combos:
            combos[key] = []
        combos[key].append(word)
    return combos           
 
def play_game(): #this is the main function to play the game
    global DEBUG
    print "Welcome to the Hangman Game!"
    print "Would you like to play the regular game (g) or the test version (t)?"
    mode = raw_input("g or t?: ")
    secLength = raw_input("How many letters would you like the secret word to contain? ")
    if int(secLength) < 3:
        print raw_input("You must choose a number of 3 or greater. Try again: ")
    wholeList = getPossibleWords("lowerwords.txt",int(secLength))
    partialList = wholeList
    secret = random.choice(wholeList) #random choice of a string in the whole lowerwords.txt file that matches the parameters
    info = ["_"]*len(secret)
    guesses = []   # letters guessed so far
    misses = 0     # misses made so far
    misses_allowed = raw_input("How many misses would you like to allow? ")
    misses_allowed = int(misses_allowed)
    win = False
    print 
    if mode == 't':
        DEBUG = True
    while misses < misses_allowed:
        if "+" in guesses:
            break
        display(guesses,info)
        print "guesses left: ",misses_allowed - misses
        print "guess a letter: "
        if DEBUG: 
            print "The secret word is:", secret.upper()
        guess = raw_input()
        if guess == "+":
            word = raw_input("guess the word: ")
            if word == secret:
                misses = misses_allowed
                win = True
            elif len(word) != len(secret):
                word = raw_input("The word needs to be the same length as the secret word. Please guess the word again: ")
                if word == secret:
                    misses = misses_allowed
                    win = True
                else:
                    guesses.append(guess)
                    misses = misses_allowed    
            else:
                guesses.append(guess)
                misses = misses_allowed
        else:
            guesses.append(guess)
            print ""
            combos = process(info, guess, partialList) #stores keys and values into the dictionary combos
            aList = [(t,len(combos[t])) for t in combos.keys()] # sorts the keys and items to elements in a tuple
            sortedList = sorted(aList, key=operator.itemgetter(1), reverse = True) #makes a sorted list of possible word combos from the given letter
            if DEBUG:
                print "Dictionary of Categories and their number of words:"
                for cat in sortedList:
                    print cat[0]," ",cat[1]
            partialList = combos[sortedList[0][0]]
            secret = random.choice(partialList)
            if guess in secret:
                print "You guessed a letter!"
                print
                info = changeInfo(guess,secret,info)
            else:
                print guess,"is not in the word."
                print
                misses = misses + 1
            print "Number of Possible words: ",len(partialList)
            if not "_" in info:
                win = True
                break
    if misses == misses_allowed and "+" in guesses:
        print "You guessed the wrong word. The secret word was",secret
    elif win:
        print "You win! The secret word was",secret
    elif misses == misses_allowed:
        print "Game over, you lose. The secret word was",secret
    else:
        return
 
if __name__ == '__main__':
    play_game()