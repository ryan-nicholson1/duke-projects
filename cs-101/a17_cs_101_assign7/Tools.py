'''
Created on Apr 5, 2017

@author: rdn5
'''
import random
 
DEBUG = True     #Set to False to remove unwanted print lines that are used for debugging.
 
def load_lines(filename):
    """
    read words from specified file and return a list of
    strings, each string is one line of the file
    """
    lines = []
    f = open(filename)
    for line in f.readlines():
        line = line.strip()
        lines.append(line)
    return lines
     
     
def get_words(filename, wordlength = 5):
    """
    returns a list of words having a specified length from
    the file whose name is filename.
    default length is 5 (if parameter not specified)
    """
    lines = load_lines(filename)
    wlist = [w for w in lines if len(w) == wordlength]
    return wlist
 
def startUp():
    global DEBUG
    print "what's the word length? ",
    n = int(raw_input())
    words = get_words("lowerwords.txt",n)
    if DEBUG: print "read %d words whose length is %d" % (len(words),n)
     
    one = random.choice(words)  #Handy method in random that pics a random word.  
     
    if DEBUG: print "choosing at random: ",one
     
    list_TOOfew(words)
 
 
 
def list_TOOfew(words):
    """ Prints out the number and the words themselves if 
        there are too few of them. 
        This makes it easier to see what happens when the game is
        about to end.
    """
    if len(words) < 30:
        for i,w in enumerate(words):
            print i,"\t",w
if __name__ == '__main__':
    pass