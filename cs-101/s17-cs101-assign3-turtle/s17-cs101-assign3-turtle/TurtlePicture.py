'''
Created on Feb 6, 2017

@author: turt Nicholson
@netid: rdn5
'''
import turtle
wn = turtle.Screen()
alex = turtle.Turtle()


def hexagonFlower(turt):
    # draw a flower made out of hexagons with the center
    # in the middle of the star. The abstract shape will
    # be completely filled with red
    turt.goto(0,0)
    turt.fillcolor("red")
    turt.begin_fill()
    for i in range(12):
        turt.forward(50)
        turt.left(60)
        turt.forward(50)
        turt.left(60)
        turt.forward(50)
        turt.left(60)
        turt.forward(50)
        turt.left(60) 
        turt.forward(50)
        turt.left(60) 
        turt.forward(50)
        turt.left(60)  
        turt.left(30)
    turt.end_fill()
    return
def star(turt):
    # this function has alex draw a 5-pointed
    # star and fills the star with the color yellow
    turt.goto(0,0)
    turt.up()
    turt.left(126)
    turt.forward(100)
    turt.left(144)
    turt.down()
    turt.color("yellow")
    turt.fillcolor("yellow")
    turt.begin_fill()
    for side in range(5):
        turt.forward(100)
        turt.right(144)
        turt.forward(100)
        turt.right(72-144)
    turt.end_fill()
    return
def square(turt):
    # this function draws a square with four different colored
    # sides: green, pink, purple, and light blue
    turt.goto(0,0)
    turt.up()
    turt.forward(193.137)
    turt.right(90)
    turt.forward(100)
    turt.left(180)
    turt.down
    for color in ["green","pink","purple","light blue"]:
        turt.color(color)
        turt.forward(200)
        turt.left(90)
    return
  
def octogon(turt):
    # this function draws an octogon
    turt.goto(0,0)
    turt.up()
    turt.forward(193.137)
    turt.right(90)
    turt.forward(80)
    turt.left(180)
    turt.down
    turt.color("blue")
    turt.fillcolor("blue")
    turt.begin_fill()
    for side in range(8):
        turt.forward(160)
        turt.left(45)
    turt.end_fill()
    return
def starSpiral(turt):
    # this function draws a spiraling star 20 times
    turt.pencolor("red")
    for x in range(20):
        turt.forward(x * 10)
        turt.right(144)
    turtle.done()
    return
    
turtle.done()
def draw():
    # main function
    starSpiral(alex)
    return
 
 
if __name__ == '__main__':
    # main function to have a turt draw a picture 
    pass
    draw()
    wn.exitonclick()

    

