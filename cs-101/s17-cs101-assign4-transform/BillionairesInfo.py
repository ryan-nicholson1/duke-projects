'''
Created on Mar 29, 2017

@author: rdn5
'''
import urllib
from collections import defaultdict
if __name__ == '__main__':
    f = urllib.urlopen("http://www.cs.duke.edu/courses/spring17/compsci101/data/billionaireData.txt").read()
    file = f.split("\n")
    billionaires = {} # creates a dictionary of billionaires as keys and their attributes as items
    countries = {} # creates a dictionary of countries as keys and female billionaires as items
    alist = []
    for line in file: # this splits the file and turns it into an iterable list of strings
        aBillionaire = line.split("$")
        alist.append(aBillionaire)
    alist = alist[0:-1]
    for person in alist: #stores attributes into billionaires
        name = person[4]
        billionaires[name] = [person[0],person[1],person[2],person[3],person[5]]
    for person in alist: # This is for Question 1. Reads over alist and stores female billionaires in their respective countries
        country = person[5]
        if person[3] == "female":
            if country not in countries: 
                countries[country] = [person[4]]
            else:
                countries[country].append(person[4])
        else:
            pass
    info = countries.items() # this section sorts the info in the dictionary
    tosort = [(len(t[1]),t[0]) for t in info] 
    countries = sorted(tosort)
    print "The country with the most female billionaires:"
    print countries[-1]
    print 
    countriesRank = {} # create a dictionary for countries with their billionaires ranks as items
    for person in alist: # places ranks in the given country key
        country = person[5]
        if country not in countriesRank: 
            countriesRank[country] = [int(person[2])]
        else:
            countriesRank[country].append(int(person[2]))
    info = countriesRank.items() # this section sorts the info in the dictionary
    tosort = [((sum(t[1])/len(t[1])),t[0]) for t in info] 
    countriesRank = sorted(tosort)
    print "The country with the lowest average rank:"
    print countriesRank[0]
    print
    netWorthPeople = {} # creates a dictionary with net worth as the key and names as the items
    netWorthCountries = {} # creates a dictionary with net worth as the key and countries as the items
    for person in alist: # the for loop to do those things
        amount = person[0]
        if amount not in netWorthPeople:
            netWorthPeople[amount] = [person[4]]
            netWorthCountries[amount] = [person[5]]
        else:
            netWorthPeople[amount].append(person[4])
            netWorthCountries[amount].append(person[5])
    info = netWorthPeople.items() # this section sorts the info in the dictionary
    tosort = [(len(t[1]),t[0]) for t in info] 
    netWorthPeople = sorted(tosort)
    blist = set(netWorthCountries['1.1']) #This is a set of the countries with 1.1 net worth, to get unique countries
    print "The amount of people with the most popular net worth in billions:"
    print netWorthPeople[-1]
    print
    print "The unique countries with a billionaire of 1.1 net worth in billions:"
    for country in blist: # printing the list(set)
        print country
    
    
        
            
    
        