'''
Created on Feb 12, 2017

@author: rdn5
'''
def isVowel(ch):
    if "aeiou".count(ch) >= 1:
        return True
    else:
        return False
    
def isConsonant(ch):
    return not isVowel(ch)

def pigifyword(word):
    '''
    returns a piglatin-ized word 
    of string parameter word
    '''
    if isVowel(word[0]):
        return word + "-way"
    elif isConsonant(word[0]):
        if word[0]=="q":
            if not word[1]=="u":
                return word[1:len(word)]+"-quay"
            else:
                return word[2:len(word)]+"-quay"
        else:
            for t in range(1,len(word)):
                if isConsonant(word[t]):
                    pass
                else:
                    return word[t:len(word)]+"-"+word[0:t]+"ay"
                    break
            return word + "-way"
    
def pigifyall(phrase):
    '''
    returns a piglatin-ized string
    of string parameter phrase
    '''
    # call pigifyword for each word in phrase
    all = []
    for word in phrase.split():
        all.append(pigifyword(word))
    return ' '.join(all)

def readFile(fname):
    '''
    returns a list of words read from file
    specified by fname
    '''
    f = open(fname)
    st = f.read()
    f.close()
    return st.split()

def writeFile(words, fname):
    '''
    YOU DO NOT NEED TO MODIFY THIS FUNCTION
    write every element in words, a list of strings
    to the file whose name is fname
    put a space between every word written, and make
    lines have length 80
    '''
    LINE_SIZE = 80
    f = open(fname,"w")
    wcount = 0
    for word in words:
        f.write(word)
        wcount += len(word)
        if wcount + 1 > LINE_SIZE:
            f.write('\n')
            wcount = 0
        else:
            f.write(' ')
    f.close()
    
if __name__ == '__main__':
    # start with reading in data file
    filename = "http://www.cs.duke.edu/courses/compsci101/spring17/assign/assign4-transform/code/romeo.txt"
    # wordlist is a list of words from the file
    wordlist = readFile(filename)
    print "read",len(wordlist),"words from file",filename 
    
    # result is the file as one long string
    result = ' '.join(wordlist)
    
    # convert to piglatin and write to file
    pigstr = pigifyall(result)
    filenameparts = filename.split(".")
    filenamepig = filenameparts[0]+"-pig."+filenameparts[1]
    writeFile(pigstr.split(),filenamepig)
    print "PIGIFIED " + filename + " as "+ filenamepig
    print "Here are the first 100 characters"
    print pigstr[0:100]  # just print first 100 chars

    # ****** replace comments with code ******
    # read in pigified file
    # ADD CODE HERE
    # unpigify pigified file that was read
    # ADD CODE HERE to print the first 
    #   100 characters of unpigified text
    
    # write unpigify text to  "unpig" filename
    # print that you have created the new file and its name
    # ADD CODE HERE
