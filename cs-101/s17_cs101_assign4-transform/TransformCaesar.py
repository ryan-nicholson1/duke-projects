'''
Created on Feb 21, 2017

@author: Ryan Nicholson
@netid: rdn5
'''
from sympy.physics.units import second
from encore.storage.tests.static_url_store_test import count

def readFile(fname):
    '''
    returns a list of words read from file
    specified by fname
    '''
    f = open(fname)
    st = f.read()
    f.close()
    return st.split()

def writeFile(words, fname):
    '''
    YOU DO NOT NEED TO MODIFY THIS FUNCTION
    write every element in words, a list of strings
    to the file whose name is fname
    put a space between every word written, and make
    lines have length 80
    '''
    LINE_SIZE = 80
    f = open(fname,"w")
    wcount = 0
    for word in words:
        f.write(word)
        wcount += len(word)
        if wcount + 1 > LINE_SIZE:
            f.write('\n')
            wcount = 0
        else:
            f.write(' ')
    f.close()
    
def encryptword(str, shift):
    # This function encrypts a string into caesar
    alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    alphLower = alph.lower()
    word = ""
    second = alph[:shift]
    first = alph[shift:]
    caesar = first + second
    for ch in str:
        if ch.isalpha():
            if ch.isupper():
                findex = alph.find(ch)
                letter = caesar[findex]
                word += letter
            else:
                findex = alphLower.find(ch)
                letter = caesar[findex]
                word += letter
        else:
            word += ch
    return word

def encrypt(phrase, shift):
    #This function encrypts a phrase
    answer = []
    for word in phrase.split():
        answer.append(encryptword(word,shift))
    return ' '.join(answer)

def eyeball(encrypted):
    #This function gives the iteration of every 
    #shift and makes the programmer find the correct
    #phrase
    alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    alphLower = alph.lower()
    phrase = ""
    alist = []
    blist = []
    clist = []
    count = 0
    for i in range(len(alph)):
        second = alph[:i]
        first = alph[i:]
        caesar = first + second
        caesarLower = caesar.lower()
        blist.append(caesar)
        clist.append(caesarLower)
    for i in blist:
        for ch in encrypted:
            if ch.isalpha():
                if ch.isupper():
                    findex = alph.find(ch)
                    letter = blist[count][findex]
                    phrase += letter
                else:
                    findex = alphLower.find(ch)
                    letter = clist[count][findex]
                    phrase += letter
            else:
                phrase += ch
        count += 1
        alist.append(phrase)
        phrase = ""   
    return "\n".join(alist)
string = "Cywodswoc sd'c okci dy myexd pbyw 1-10, led xyd kvgkic"


def decrypt(encrypted):
    #This function decrypts a message in caesar
    astring = eyeball(encrypted)
    alist = astring.split('\n')
    count = 0
    maxVal = 0
    filename = "melville.txt"
    wordlist = readFile(filename) 
    for x in alist:
        aline = x.split()
        for word in aline:
            if word in wordlist:
                count += 1
            else:
                pass
        if count > maxVal:
            maxVal = count
            phrase = x
        else:
            maxVal = maxVal
        count = 0
    return phrase               

        

if __name__ == '__main__':
    grocery = "I'M GOING TO THE GROCERY STORE"
    print "Phrase 'I'M GOING TO THE GROCERY STORE' encrypted with a shift of 7:"
    print
    print encrypt(grocery, 7)
    print
    print "Output of the function eyeball:"
    print
    print eyeball(string)
    print
    filename = "file1-encr.txt"
    #filename = "file2-encr.txt"
    # wordlist is a list of words from the file
    wordlist = readFile(filename)
    # result is the file as one long string
    result = ' '.join(wordlist)
    answer = decrypt(result)
    filename1 = "file1-encr-unencr.txt"
    writeFile(answer.split(), filename1)
    print "Decrypted file 1:"
    print
    print filename1
    print
    filename2 = "file2-encr.txt"
    # wordlist is a list of words from the file
    wordlist2 = readFile(filename2)
    # result is the file as one long string
    result2 = ' '.join(wordlist2)
    answer2 = decrypt(result2)
    filename3 = "file2-encr-unencr.txt"
    writeFile(answer2.split(), filename3)
    print "Decrypted file 2:"
    print
    print answer2
    pass