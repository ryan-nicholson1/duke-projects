'''
Created on February 6, 2017
@author: YOUR NAME HERE
'''
from __builtin__ import str
from StdSuites.AppleScript_Suite import string
from test.test_xml_etree import writefile
from enclosure.testing.mock_license_server import prefix
from findertools import restart

def isVowel(ch):
    #returns whether a character is a vowel
    if "aeiou".count(ch) >= 1:
        return True
    elif "AEIOU".count(ch) >= 1:
        return True
    else:
        return False
    
def isConsonant(ch):
    # returns whether a character is a consonant
    return not isVowel(ch)

def pigifyword(word):
    '''
    returns a piglatin-ized word 
    of string parameter word
    '''
    if isVowel(word[0]):
        if word.isupper():
            return word + "-WAY"
        else:
            return word + "-way"
    if isConsonant(word[0]) and not word[0].isupper():
        if word[0]=="q":
            if not word[1]=="u":
                return word[1:len(word)-1]+"-quay"
            else:
                return word[2:len(word)-1]+"-quay"
        else:
            for t in range(1,len(word)):
                if word[0].isupper():
                    if word[t] == "y":
                        return word[t:len(word)].capitalize() + "-" + word[0:t] + "ay"
                    if isVowel(word[t]) and not t == 0:
                        return word[t:len(word)].upper() + "-" + word[0:t] + "ay"
                    if isVowel(word[t]):
                        return word[t:len(word)].capitalize() + "-" + word[0:t] + "ay"
                    if isConsonant(word[t]):
                        pass
    if word.isupper():
        if word[0]=="q":
            if not word[1]=="u":
                return word[1:len(word)-1]+"-quay"
            else:
                return word[2:len(word)-1]+"-quay"
        else:
            for t in range(1,len(word)):
                if word[t] == "y":
                    return word[t:len(word)].upper() + "-" + word[0:t] + "AY"
                if isVowel(word[t]) and not t == 0:
                    return word[t:len(word)].upper() + "-" + word[0:t] + "AY"
                if isVowel(word[t]):
                    return word[t:len(word)].upper() + "-" + word[0:t] + "AY"
                if isConsonant(word[t]):
                    pass
    if word[0].isupper():
        if word[0]=="Q":
            if not word[1]=="u":
                return word[1:len(word)-1].capitalize() + "-quay"
            else:
                return word[2:len(word)-1].capitalize() + "-quay"  
        for t in range(1,len(word)):
            if word[t] == "Y":
                return word[t:len(word)].capitalize() + "-" + word[0:t].lower() + "ay"
            if isVowel(word[t]) and not t == 0:
                return word[t:len(word)].capitalize() + "-" + word[0:t].lower() + "ay"
            if isVowel(word[t]):
                return word[t:len(word)].capitalize() + "-" + word[0:t].lower() + "ay"
            if isConsonant(word[t]):
                pass         
        
    
def pigifyall(phrase):
    '''
    returns a piglatin-ized string
    of string parameter phrase
    '''
    # call pigifyword for each word in phrase
    all1 = []
    all2 = []
    for i in phrase.split():
        all1.append(pigifyword(i))
    for x in all1:
        if type(x) == str:
            all2.append(x)
    return ' '.join(all2)

def unpigifyword(word):
    # transforms a string in pig latin
    # back to its original value in
    # english
    word = word.strip()
    alist = word.split("-")
    rest = alist[0]
    prefixAY = alist[1]
    if prefixAY == "WAY" or prefixAY == "way":
        return rest
    else:
        prefix = prefixAY[0:-2]
        if rest.isupper() or rest.islower():
            return prefix + rest
        else:
            return prefix.capitalize() + rest.lower()
        
def unpigifyall(phrase):
    '''
    returns a string un- piglatinized
    of string parameter phrase
    '''
    # call unpigifyword for each word in phrase
    all1 = []
    all2 = []
    for i in phrase.split():
        all1.append(unpigifyword(i))
    for x in all1:
        if type(x) == str:
            all2.append(x)
    return ' '.join(all2)
        
def readFile(fname):
    '''
    returns a list of words read from file
    specified by fname
    '''
    f = open(fname)
    st = f.read()
    f.close()
    return st.split()

def writeFile(words, fname):
    '''
    YOU DO NOT NEED TO MODIFY THIS FUNCTION
    write every element in words, a list of strings
    to the file whose name is fname
    put a space between every word written, and make
    lines have length 80
    '''
    LINE_SIZE = 80
    f = open(fname,"w")
    wcount = 0
    for word in words:
        f.write(word)
        wcount += len(word)
        if wcount + 1 > LINE_SIZE:
            f.write('\n')
            wcount = 0
        else:
            f.write(' ')
    f.close()
    
if __name__ == '__main__':
    # start with reading in data file
    filename = "romeo.txt"
    # wordlist is a list of words from the file
    wordlist = readFile(filename)
    print "read",len(wordlist),"words from file",filename 
    
    # result is the file as one long string
    result = ' '.join(wordlist)
    
    # convert to piglatin and write to file
    pigstr = pigifyall(result)
    filenameparts = filename.split(".")
    filenamepig = filenameparts[0]+"-pig."+filenameparts[1]
    writeFile(pigstr.split(),filenamepig)
    print "PIGIFIED " + filename + " as "+ filenamepig
    print "Here are the first 100 characters"
    print pigstr[0:100]  # just print first 100 chars

    print
    print

    pigWordlist = readFile(filenamepig)
    print "read",len(pigWordlist),"words from file",filenamepig
    
    pigResult = ' '.join(pigWordlist)

    #convert from pig latin and write to a new file
    unpigstr = unpigifyall(pigResult)
    pigFilenameparts = filenamepig.split(".")
    filenameunpig = pigFilenameparts[0]+"-unpig"+pigFilenameparts[1]
    writeFile(unpigstr.split(), filenameunpig)
    print "UNPIGIFIED " + filenamepig + " as " + filenameunpig
    print "Here are the first 100 characters"
    print unpigstr[0:100]
    


