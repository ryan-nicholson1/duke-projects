'''
Created on Feb 26, 2017

@author: YOUR NAME HERE
'''

import random

def fileToStringList(filename):
    """
    filename is a file, 
    returns a list of strings, each string represents
    one line from filename
    """
    wordlist = []
    f = open(filename)
    for line in f:
        line = line.strip()
        wordlist.append(line)
    f.close()
    return wordlist
    
     
def getPossibleWords(wordlist,length):
    """
    returns a list of words from wordlist having a 
    specified length 
    """
    wordlist = [w for w in wordlist if len(w) == length]
    # only those words of the specified length
    return wordlist

def displayGuess(wordList):
    '''
    wordList is a list of characters with letters correctly
    guessed and '_' for letters not quessed yet
    returns the list as a String
    '''
    return ' '.join(wordList)

def guessStart(word):
    '''
    returns a list of single characters '_' the
    same size as word
    '''
    return ['_']*len(word)

def updateLetter(guessList,wordToGuess, letter):
    '''
    wordToGuess is the word the user is trying to guess.
    guessList is the word to guess as a list of characters, but
    only including the letters the user has guessed and showing
    the character '_' if a letter hasn't been guessed yet.
    letter is the current letter the user has guessed. 
    
    Modify guessList to include letter in its proper locations if 
    letter is in wordToGuess.
    
    For example, if the wordToGuess is "baloney" and so far only a and
    e have been guessed, then guessList is ['_','a','_','_','_','e','_']
    If letter is 'o', then guessList is modified to now be:
    ['_','a','_','o','_','e','_']
    
    '''
    # TODO: replace pass with code to modify guessList if letter is in
    #  the wordToGuess
    for index in range(len(wordToGuess)):
        if wordToGuess[index].lower() == letter.lower():
            guessList[index] = wordToGuess[index]
    return guessList



def playGame(words):
    '''
    Play the game. Let the user know if they won or not.
    '''
    #setup for game
    guessLength = int(raw_input("how many letters in word to guess? "))
    wordsOfLength = getPossibleWords(words,guessLength)    
    wordToGuess = random.choice(wordsOfLength)
    guessList = guessStart(wordToGuess)
    misses = int(raw_input("how many misses to allow? ")) #Number of allowed misses until user loses the game
    lettersGuessed = ""
    # start the guessing
    while True:
        if misses == 0: #User has guessed too many incorrect letters
            break
        if guessList.count('_') == 0:
            # all letters guessed
            break
        print # output to print for each round
        print "guessed so far:", displayGuess(guessList)
        letter = raw_input("guess a letter: ")
        if letter == "+":
            word = raw_input("guess the word: ")
            if word == wordToGuess:
                print "You win. You guessed the word", wordToGuess
            elif len(word) < 3:
                word == raw_input("The word needs to be 3 letters or more. Please guess the word again: ")
                if word == wordToGuess:
                    print "You win. You guessed the word", wordToGuess     
                else:
                    print "You lost, word was", wordToGuess           
            else:
                print "You lost, word was", wordToGuess
        updateLetter(guessList, wordToGuess, letter)
        if letter.lower() not in wordToGuess: #To modify the misses left until game is over
            misses += -1
            if letter in lettersGuessed:
                print "You have already guessed this letter"
        lettersGuessed += letter
        print "".join(sorted(lettersGuessed))
        print misses
        # game over
    if guessList.count('_') == 0:
        print "You win. You guessed the word", wordToGuess
    else:
        print "You lost, word was", wordToGuess


if __name__ == '__main__':
    words = fileToStringList('lowerwords.txt')
    playGame(words)