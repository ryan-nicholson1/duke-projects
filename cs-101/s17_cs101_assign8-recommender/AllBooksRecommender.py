'''
Created on Apr 20, 2017

@author: rdn5
'''

from ReadAllBooks import processData
from RecommenderFunctions import average, similarities, recommended

if __name__ == '__main__':
    data = processData('BooksAuthorsForRatings.txt','RatingsAllBooks.txt')
    itemlist = data[0]
    dictratings = data[1]
    resultAVG = average(itemlist, dictratings)
    person = "Rus"
    resultSim = similarities(person, dictratings)
    print recommended(resultSim, itemlist, dictratings, 4)