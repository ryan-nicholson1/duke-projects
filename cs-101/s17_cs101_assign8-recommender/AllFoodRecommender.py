'''
Created on Apr 19, 2017

@author: rdn5
'''
from ReadAllFood import processData
from RecommenderFunctions import average, similarities, recommended

if __name__ == '__main__':
    data = processData('RatingsAllFood.txt')
    itemlist = data[0]
    dictratings = data[1]
    resultAVG = average(itemlist, dictratings)
    person = "Kim Lee"
    resultSim = similarities(person, dictratings)
    print recommended(resultSim, itemlist, dictratings, 4)
