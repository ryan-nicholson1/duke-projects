'''
Created on Apr 20, 2017

@author: rdn5
'''

def processData(booktitles, bookratings):
    f = open(booktitles)
    itemlist = []
    for line in f:
        alist = line.strip().split("[")
        author = alist[2][:-1]
        title = alist[1][:-1]
        itemlist.append(title+","+author)
    f.close()
    f = open(bookratings)
    blist = []
    for line in f:
        line = line.strip()
        blist.append(line)
    dictratings = {}
    for i in range(len(blist)):
        if i % 2 == 1:
            clist = blist[i].split(":")
            clist = [int(x) for x in clist]
            dictratings[blist[i-1]] = clist
    return (itemlist,dictratings)

if __name__ == '__main__':
    print processData('BooksAuthorsForRatings.txt','RatingsAllBooks.txt')