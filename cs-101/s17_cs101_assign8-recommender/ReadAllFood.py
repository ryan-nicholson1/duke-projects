'''
Created on Apr 19, 2017

@author: rdn5
'''
def processData(filename):
    itemlist = []
    f = open(filename)
    for line in f:
        (name,ratings) = line.split(":")
        ratingslist = ratings.split()
        for x in range(len(ratingslist)):
            if x % 2 == 0:
                if ratingslist[x] not in itemlist:
                    itemlist.append(ratingslist[x])
    f.close()
    f = open(filename)
    dictratings = {}
    for line in f:
        (name, a) = line.split(":")
        alist = a.split()
        dictratings[name] = []
        for rest in itemlist:
            if rest in alist:
                idx = alist.index(rest) + 1
                dictratings[name].append(int(alist[idx]))
            else:
                dictratings[name].append(0)
    return (itemlist, dictratings)
                
if __name__ == '__main__':
    print processData('RatingsAllFood.txt')