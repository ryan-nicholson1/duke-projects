'''
Created on Apr 20, 2017

@author: rdn5
'''

def processData(filename):
    f = open(filename)
    alist = [line for line in f]
    dictratings = {}
    itemlist = []
    for i in range(len(alist)):
        if i % 2 == 1:
            blist = alist[i].split()
            rating = blist[-1]
            if alist[i-1] not in dictratings:
                dictratings[alist[i-1].strip()] = [int(rating)]
            else:
                dictratings[alist[i-1].strip()].append(int(rating))
            del blist[-1]
            if len(blist) > 1:
                itemlist.append(" ".join(blist))
            else:
                itemlist.append(blist[0])
    return (itemlist, dictratings)
        

if __name__ == '__main__':
    (itemlist, dictratings) = processData('RatingsAllMovies.txt')
    print dictratings