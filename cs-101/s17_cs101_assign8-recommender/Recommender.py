'''
Created on Apr 18, 2017

@author: rdn5
'''
 
import operator
 
_booklist = []
_bookratings = {}
 
_movielist = []
_movieratings = {}
 
_simtups = []
 
def get_book_data(books="books.txt", ratings="bookratings.txt"):
     
    global _booklist
    file = open(books)
    _booklist = [ word.strip().split(",")[1] for word in file ]
    file.close()
     
    global _bookratings
    ratings_list = []
    file = open(ratings)
    line_count = 0
     
    for line in file:
        ratings_list.append(line.strip().split())
        line_count +=1
    i = 0
    #print ratings_list
    while i < line_count:
        _bookratings[ratings_list[i][0]] = []
        for item in ratings_list[i+1]:
            _bookratings[ratings_list[i][0]].append(item)
        i+=2
         
    file.close()
    print _booklist
    #print _bookratings
    #return _booklist, _bookratings
     
def get_movie_data(movieratings="movieratings.txt"):
    global _movielist
    file = open(movieratings)
    line_count = 0
    big_list = []
    student_list = []
    for line in file:
        line_count +=1
        big_list.append(line.split())
        _movielist.append(line.split()[1])
        student_list.append(line.split()[0])
      
    student_list = sorted(list(set(student_list)))
    file.close()
     
    file= open(movieratings)
    global _movieratings
    _movielist = list(set(_movielist))
 
    for student in student_list:
        _movieratings[student] = [0]*len(_movielist)
     
    for line in file:
        movie_num = _movielist.index(line.split()[1])
        rating = line.split()[2]
        user = line.split()[0]
        _movieratings[user][movie_num] = rating
             
         
    file.close()
     
    print _movielist
    #print _movieratings
     
    #return _movielist, _movieratings
 
 
def average(items, ratings):
    ratings_dict = {}
    for item in items:
        book = item
        ratings_dict[book] = []
        for person in ratings:
            if ratings[person][items.index(item)] != '0':
                ratings_dict[book].append(int(ratings[person][items.index(item)]))
#    print ratings_dict
    for entry in ratings_dict:
        length = len(ratings_dict[entry])
        total = sum(ratings_dict[entry])
        if length != 0:
            ratings_dict[entry] = total/length
    ratings_tup = ratings_dict.items()
    ratings_tup = sorted(ratings_tup, key=operator.itemgetter(1), reverse=True)
    #print ratings_tup
     
def similarities(name, ratings):
    global _simtups
    sim_dict = {}
    name_ratings = ratings[name]
    print name_ratings
    for user in ratings:
        sim_dict[user] = 0
        user_ratings = ratings[user]
        for i in range(len(user_ratings)):
            sim_dict[user]+= int(user_ratings[i])*int(name_ratings[i])
    _simtups = sim_dict.items()
    _simtups = sorted(_simtups, key=operator.itemgetter(1), reverse=True)
    print _simtups
     
def recommended(items, ratings, similarities, n):
    similar_users = similarities[:n]
#    print similar_users
    weighted_dict = {}
#    print ratings
    for user in similar_users:
        weighted_dict[user[0]] = []
        temp_ratings = ratings[user[0]]
        for rating in temp_ratings:
            weighted_dict[user[0]].append( int(rating)*user[1] )
    w_movie_dict = {}
    for entry in weighted_dict:
        for i in range(len(weighted_dict[entry])):
            key = items[i]
            #print key
            rating = weighted_dict[entry][i]
            if not key in w_movie_dict:
                w_movie_dict[key] = []
            w_movie_dict[key].append(rating)
#            print rating
#            print w_movie_dict
#    print weighted_dict
#    print w_movie_dict
    rec_tups = [(x,sum(y)) for x,y in w_movie_dict.items()]
    rec_tups = sorted(rec_tups, key=operator.itemgetter(1), reverse=True)
    print rec_tups
     
 
if __name__ == "__main__":
    #get_book_data()
    get_movie_data()
    #average(_booklist, _bookratings)
    average(_movielist, _movieratings)
    #similarities("Cust1", _bookratings)
    similarities("student1250", _movieratings)
    #recommended(_booklist, _bookratings, _simtups, 5)
    recommended(_movielist, _movieratings, _simtups, 5)
                