'''
Created on Apr 19, 2017

@author: rdn5
'''
import operator

def average(itemlist, dictratings):
    ratings_dict = {}
    for item in itemlist:
        x = item
        ratings_dict[x] = []
        for person in dictratings:
            if dictratings[person][itemlist.index(item)] != '0':
                ratings_dict[x].append(int(dictratings[person][itemlist.index(item)]))
    for entry in ratings_dict:
        length = len(ratings_dict[entry])
        total = sum(ratings_dict[entry])
        if length != 0:
            ratings_dict[entry] = total/length
    averageList = ratings_dict.items()
    averageList = sorted(averageList, key=operator.itemgetter(1), reverse=True)
    return averageList
    #print averageList
 
def similarities(name, dictratings):
    sim_dict = {}
    name_ratings = dictratings[name]
    #print name_ratings
    for user in dictratings:
        sim_dict[user] = 0
        user_ratings = dictratings[user]
        for i in range(len(user_ratings)):
            sim_dict[user]+= int(user_ratings[i])*int(name_ratings[i])
    similarList = sim_dict.items()
    similarList = sorted(similarList, key=operator.itemgetter(1), reverse=True)
    return similarList
    
def recommended(similarities, itemlist, ratings, n):
    similar_users = similarities[:n]
#    print similar_users
    weighted_dict = {}
#    print ratings
    for user in similar_users:
        weighted_dict[user[0]] = []
        temp_ratings = ratings[user[0]]
        for rating in temp_ratings:
            weighted_dict[user[0]].append(float(rating)*user[1])
    w_movie_dict = {}
    for entry in weighted_dict:
        for i in range(len(weighted_dict[entry])):
            key = itemlist[i]
            #print key
            rating = weighted_dict[entry][i]
            if not key in w_movie_dict:
                w_movie_dict[key] = []
            w_movie_dict[key].append(rating)
#            print rating
#            print w_movie_dict
#    print weighted_dict
#    print w_movie_dict
    recommendList = [(x,sum(y)) for x,y in w_movie_dict.items()]
    recommendList = sorted(recommendList, key=operator.itemgetter(1), reverse=True)
    return recommendList
    
   
if __name__ == '__main__':
    pass