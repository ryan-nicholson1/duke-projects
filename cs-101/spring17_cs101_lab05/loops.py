'''
Created on Feb 19, 2017

@author: Ryan Nicholson
'''

def addDigitLeft(number, digit):
    if number == 0:
        return digit
    return int(str(digit)+str(number))

def digitsSmallerThanSum(number, sum):
    # return digits from right side that are less than sum
    answer = 0
    if sum == 0:
        return 0
    lastdigit = number % 10
    
    sumDigitsSoFar = lastdigit
    if sumDigitsSoFar > sum:
        return 0
    newnumber = lastdigit
    numString = str(number)
    pos = len(numString) - 1
    while True:  
        answer = newnumber
        answerStr = str(answer)
        answerSum = 0
        for ch in answerStr:
            answerSum = answerSum + int(ch)# last number that worked
        if answerSum <= sum:
            newnumber = addDigitLeft(answer, numString[pos - 1])
            pos -= 1
            True
        else:
            answer = newnumber
            break
    if len(str(answer)) != 1:
        answer = int(str(answer)[1:])
    return answer



def findFirst(phrase, letter):
    # return the first position of letter in phrase
    # or -1 if it is not found
    # -- Do not use string .find
    # -- Use a WHILE and break
    if len(phrase)==0:
        return -1
    pos = 0
    while True:
        if letter not in phrase:
            pos = -1
            break
        if letter == phrase[pos]:
            pos
            break
        else:
            True
        pos += 1
    return pos
def findLast(phrase, letter):
    # return the last position of letter in phrase
    # or -1 if it is not found
    if len(phrase)==0:
        return -1
    pos = len(phrase) - 1
    while True:
        if letter not in phrase:
            pos = -1
            break
        if letter == phrase[pos]:
            pos
            break
        else:
            True
            pos -= 1
    return pos




if __name__ == '__main__':
    print "Find first occurrence of letter"
    print "a", findFirst("The tragedy of Romeo and Juliet", "a")
    print "m", findFirst("The tragedy of Romeo and Juliet", "m")
    print "z", findFirst("The tragedy of Romeo and Juliet", "z")
    print "t", findFirst("The tragedy of Romeo and Juliet", "t")

    print "Find last occurrence of letter"    
    print "a", findLast("The tragedy of Romeo and Juliet", "a")
    print "m", findLast("The tragedy of Romeo and Juliet", "m")
    print "z", findLast("The tragedy of Romeo and Juliet", "z")
    print "t", findLast("The tragedy of Romeo and Juliet", "t")

    print "digitsSmallerThanSum(5372173, 11)", digitsSmallerThanSum(5372173, 11) 
    print "digitsSmallerThanSum(5372173, 2)", digitsSmallerThanSum(5372173, 2)
    print "digitsSmallerThanSum(5372173, 15)", digitsSmallerThanSum(5372173, 15)
    print "digitsSmallerThanSum(55555555, 5)", digitsSmallerThanSum(55555555, 5)




