;; Ryan Nicholson
;; Problem Set #1
;; September 6, 2017
;; CS 230


;;; Go to Language, Choose Language, Other Languages, Swindle, Full Swindle
;;; This may have to be done in cs230-graphics.scm as well
;;; cs230.ps1.scm


(require racket/base) ;;This allows the type system to work.
(require (file "cs230-graphics.scm")) ;;Pull in the definitions for the drawing window and stuff. Assumes the file is in the same directory. 

;; Here are the procedures you will modify in the problem set
(define side
  (lambda ((length <real>) (heading <real>) (level <integer>))
    (if (zero? level)
        (drawto heading length)
        (let ((len/3 (/ length 3))
              (lvl-1 (- level 1)))
          (side len/3 heading lvl-1)
          (side len/3 (- heading PI/3) lvl-1)
          (side len/3 (+ heading PI/3) lvl-1)
          (side len/3 heading lvl-1)))))

(define snowflake:0
  (lambda ((length <real>) (level <integer>))
    (side length 0.0 level)
    (side length (* 2 PI/3) level)
    (side length (- (* 2 PI/3)) level)))

;; PROBLEM 1 ;;

(define flip-side
  (lambda ((length <real>) (heading <real>) (level <integer>))
    (if (zero? level)
        (drawto heading length)
        (let ((x (/ length 1.414))
              (lvl-1 (- level 1)))
          (flip-side x (- heading PI/4) lvl-1)
          (flip-side (* 2 x) (+ heading PI/4) lvl-1)
          (flip-side x (- heading PI/4) lvl-1)))))

(define square-snowflake:1
  (lambda ((length <real>) (level <integer>))
    (flip-side length 0.0 level)
    (flip-side length PI/2 level)
    (flip-side length PI level)
    (flip-side length (* 3 PI/2) level)))

;; PROBLEM 2 ;;

(define snowflake:2
  (lambda ((length <real>) (level <integer>) (fn <function>))
    (fn length 0.0 level)
    (fn length (* 2 PI/3) level)
    (fn length (- (* 2 PI/3)) level)))

(define square-snowflake:2
  (lambda ((length <real>) (level <integer>) (fn <function>))
    (fn length 0.0 level)
    (fn length PI/2 level)
    (fn length PI level)
    (fn length (* 3 PI/2) level)))

;; PROBLEM 3 ;;

(define side-inv
  (lambda ((length <real>) (heading <real>) (level <integer>))
    (if (zero? level)
        (drawto heading length)
        (let ((len/3 (/ length 3))
              (lvl-1 (- level 1)))
          (side len/3 (* -1 heading) lvl-1)
          (side len/3 (* -1 (- heading PI/3)) lvl-1)
          (side len/3 (* -1 (+ heading PI/3)) lvl-1)
          (side len/3 (* -1 heading) lvl-1)))))

(define snowflake-inv
  (lambda ((length <real>) (level <integer>) (fn <function>))
    (fn length (* -1 0.0) level)
    (fn length (* -1 (* 2 PI/3)) level)
    (fn length (* -1 (- (* 2 PI/3))) level)))

;; PROBLEM 4 ;;

(define side-length
  (lambda ((length <real>) (level <integer>))
    (let ((len/3 (/ length 3)))
    (if (zero? level)
        (length)
        (expt (* 4 len/3) level)))))

(define snowflake-length
  (lambda ((length <real>) (level <integer>) (fn <function>))
    (let ((y (* 3 fn)))
    (if (zero? level)
        (* 3 length)
        (y)))))

;;Answer: 64000000/9 ;;

;; Make the graphics window visible, and put the pen somewhere useful
(init-graphics 640 480)
(clear)
(moveto 100 100)


;(square-snowflake:1 150 3)

;(snowflake:2 200 3 side)
;(snowflake:2 30 3 flip-side)
;(square-snowflake:2 200 3 side)
;(square-snowflake:2 30 3 flip-side)

;(side-inv 50 0.0 1 -1)
;(snowflake-inv 150 3 side-inv)

;(side-length 100 3)






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

