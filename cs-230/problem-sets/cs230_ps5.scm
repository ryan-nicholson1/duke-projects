;; Jerry Li
;; Ryan Nicholson
;; PS 5

(require racket/base)
(require racket/stream)

;;------------------PART ONE: Finite Sets-------------------

;; Problem 1

(define cart
  (lambda ((A <list>) (B <list>))
    (cond ((null? A) '())
          ((append (map (lambda y (list (car A) (car y))) B)
                  (cart (cdr A) B))))))

(define a '(x y z))
(define b '(1 2 3))
(cart a b)


;; Problem 2

(define fcn?
  (lambda (S)
    (cond ((null? S) #t)
          ((null? (cdr S)) #t)
          ((and (equal? (caar S) (car (car (cdr S))))
                (not (equal? (cdar S) (cadr (cadr S))))) #f)
          ((fcn? (cdr S))))))

(fcn? '((x 1) (y 1) (z 2)))


;; Problem 3

(define pi1
  (lambda (S)
    (cond ((null? S) '())
          ((cons (caar S) (pi1 (cdr S)))))))

(define pi2
  (lambda (S)
    (cond ((null? S) '())
          ((cons (cadar S) (pi2 (cdr S)))))))
  
(pi2 '((x 1) (y 1) (z 1)))


;; Problem 4

(define diag
  (lambda (A)
    (cond ((null? A) '())
          ((map (lambda y (list (car y) (car y))) A)
          ))))

;(diag '(1 2 3 4 5))


;; Problem 5

(define diag?
  (lambda (D)
    (cond ((null? D) #t)
          ((not (equal? (caar D) (cadar D))) #f)
          ((diag? (cdr D))))))

(define w '(x x))
(define b '(1 2))

;(diag? (list w b))


;; Problem 6

(define diag-inv
  (lambda (Delta)
    (cond ((null? Delta) '())
          ((cons (caar Delta) (diag-inv (cdr Delta)))))))

(diag-inv (list w b))

;;------------------PART TWO: (Countably) Infinite Sets-------------------

(define ones  (stream-cons 1 ones))

(define integers (stream-cons 1 (add-streams ones integers)))

(define add-streams 
  (lambda ((a <stream>) (b <stream>))
    (cond ((stream-empty? a) b)
          ((stream-empty? b) a)
          (else (stream-cons (+ (stream-first a) (stream-first b))
                                (add-streams (stream-rest a) (stream-rest b)))))))

(define stream->listn
  (lambda ((s <stream>) (n <integer>))
    (cond ((or (zero? n) (stream-empty? s)) '())
          (else (cons (stream-first s)
                      (stream->listn (stream-rest s) (- n 1)))))))

;; Problem 1

(define cart-stream
  (lambda (A B)
    (letrec
        ((loop (lambda (x y counter)
                 (cond ((equals? x 0)
                        (stream-cons (list (stream-ref A x)
                                           (stream-ref B y))
                                     (loop (+ counter 1) 0 (+ counter 1))))
                       (else (stream-cons (list (stream-ref A x)
                                                (stream-ref B y))
                                          (loop (- x 1) (+ y 1) counter)))))))
      (loop 0 0 0))))

(define cart-stream-ints (cart-stream integers integers)) ;creates infinite cartesian product of integers

(stream->listn cart-stream-ints 10)

;; Problem 2

;; Problem 3

(define pi1-stream
  (lambda (S)
    (stream-cons (car (stream-first S))
                 (pi1-stream (stream-rest S)))))

(define pi2-stream
  (lambda (S)
    (stream-cons (cadr (stream-first S))
                 (pi2-stream (stream-rest S)))))

(stream->listn (pi1-stream cart-stream-ints) 10)
(stream->listn (pi2-stream cart-stream-ints) 10)

; Problem 4

(define diag-stream
  (lambda (S)
    (stream-cons (list (stream-first S) (stream-first S))
                 (diag-stream (stream-rest S)))))

(stream->listn (diag-stream integers) 10)

; Problem 5


; Problem 6

(define diag-inv-stream
  (lambda (Delta)
    (stream-cons (car (stream-first Delta))
                 (diag-inv-stream (stream-rest Delta)))))

(stream->listn (diag-inv-stream (diag-stream integers)) 10)

; Problem 7



    



