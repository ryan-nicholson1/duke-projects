; Jerry Li
; Ryan Nicholson
; PS 6

;---------------------PART 1: Warm Up!-------------------------

;;;;;;;;;;;;;;;;;;;;;;;;;Problem 1;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; P(H|fair) = 0.5
; P(T|fair) = 0.5
; P(H|biased) = 0.65
; P(T|biased)= 0.35
; P(fair) = P(biased) = 1/2

; Let seq = H H T T H T H T H T T T

; P(fair|seq) = (P(seq|fair) * P(fair)) / P(seq)
; P(biased|seq) = (P(seq|biased) * P(biased)) / P(seq)

;(seq) = P(seq|fair) * P(fair) + P(seq|biased) * P(biased)
;       = (0.0002441 * 0.5) + (0.0000747 * 0.5)
;       = 0.0001597

;P(fair|seq) = (P(seq|fair) * P(fair)) / P(seq)
;            = (0.0002441 * 0.5) / 0.0001597
;            = 0.7642

;P(biased|seq) = (P(seq|biased) * P(biased)) / P(seq)
;            = (0.0000747 * 0.5) / 0.0001597
;            = 0.2338

; I think the casino used a fair coin.

;;;;;;;;;;;;;;;;;;;;;;;;Problem 2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Let seq = H H T H H H T T H H H T

; P(fair|seq) = (P(seq|fair) * P(fair)) / P(seq)
; P(biased|seq) = (P(seq|biased) * P(biased)) / P(seq)

;P(seq) = P(seq|fair) * P(fair) + P(seq|biased) * P(biased)
;       = (0.0002441 * 0.5) + (0.0004782 * 0.5)
;       = 0.0003611

;P(fair|seq) = (P(seq|fair) * P(fair)) / P(seq)
;            = (0.0002441 * 0.5) / 0.0003611
;            = 0.338




;P(biased|seq) = (P(seq|biased) * P(biased)) / P(seq)
;            = (0.0004782 * 0.5) / 0.0003611
;            = 0.662

; I believe the casino used a biased coin.


;------------------PART 2: Game Time!-------------------------

;;;;;;;;;;;;;;;;;;;; Problem 3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(define viterbiProb1
  (lambda (emission k switchPr p1 p2)
    (cond ((and (equal? 1 k) (equal? 0 (car emission))) (* 0.5 p1))
          ((and (equal? 1 k) (equal? 1 (car emission))) (* 0.5 (- 1 p1)))
          ((equal? 0 (car emission)) (max (* (viterbiProb1 (cdr emission) (- k 1) switchPr p1 p2) (- 1 switchPr) p1) 
                                          (* (viterbiProb1 (cdr emission) (- k 1) switchPr p2 p1) switchPr p1)))                                 
          ((equal? 1 (car emission)) (max (* (viterbiProb1 (cdr emission) (- k 1) switchPr p1 p2) (- 1 switchPr) (- 1 p1))
                                          (* (viterbiProb1 (cdr emission) (- k 1) switchPr p2 p1) switchPr (- 1 p1)))))))                                           

(define viterbiProb
  (lambda (emission k switchPr p1 p2)
    (cond ((equal? 0 (car (reverse (remover emission k))))
           (max (* (viterbiProb1 (cdr (reverse (remover emission k))) (- k 1) switchPr p1 p2) (- 1 switchPr) p1)
                (* (viterbiProb1 (cdr (reverse (remover emission k))) (- k 1) switchPr p2 p1) switchPr p1)))                                            
          ((equal? 1 (car (reverse (remover emission k))))
           (max (* (viterbiProb1 (cdr (reverse (remover emission k))) (- k 1) switchPr p1 p2) (- 1 switchPr) (- 1 p1))
                (* (viterbiProb1 (cdr (reverse (remover emission k))) (- k 1) switchPr p2 p1) switchPr (- 1 p1)))))))

; something that removes last value of emission until length is k
(define remover
  (lambda (emission k)
    (cond ((equal? k (length emission)) emission)
          ((remover (reverse (cdr (reverse emission))) k)))))

;(viterbiProb '(0 0 1 1 0) 5 0.45 0.5 0.65)
;(viterbiProb '(1 0 0 0 1) 5 0.45 0.5 0.65)
;(viterbiProb '(1 0 1) 2 0.45 0.5 0.65)
;(viterbiProb '(1 1) 2 0.45 0.5 0.65)


;;;;;;;;;;;;;;;;;;;; Problem 5;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; helper function that iterates and check if length of emission is 1

; let a = viterbiprob of not switched
;     b = viterbiporb of switched

(define viterbiPath1
  (lambda (emission switchPr s1 s2 p1 p2)
    (cond ((and (equal? 1 (length emission)) (equal? 0 (car emission))) (* 0.5 p1))
          ((and (equal? 1 (length emission)) (equal? 1 (car emission))) (* 0.5 (- 1 p1)))
          ((equal? 0 (car emission)) (max (* (viterbiPath1 (cdr emission) switchPr s1 s2 p1 p2) (- 1 switchPr) p1) 
                                          (* (viterbiPath1 (cdr emission) switchPr s2 s1 p2 p1) switchPr p1)))                                 
          ((equal? 1 (car emission)) (max (* (viterbiPath1 (cdr emission) switchPr s1 s2 p1 p2) (- 1 switchPr) (- 1 p1))
                                          (* (viterbiPath1 (cdr emission) switchPr s2 s1 p2 p1) switchPr (- 1 p1)))))))                                           

(define viterbiPath
  (lambda (emission switchPr s1 s2 p1 p2)
    (cond ((equal? 0 (car (reverse emission))) (max (* (viterbiPath1 (cdr (reverse emission)) switchPr s1 s2 p1 p2) (- 1 switchPr) p1)
                                                    (* (viterbiPath1 (cdr (reverse emission)) switchPr s2 s1 p2 p1) switchPr p1)
                                                    (* (viterbiPath1 (cdr (reverse emission)) switchPr s2 s1 p2 p1) switchPr p2)
                                                    (* (viterbiPath1 (cdr (reverse emission)) switchPr s1 s2 p1 p2) (- 1 switchPr) p2)))                                            
          ((equal? 1 (car (reverse emission))) (max (* (viterbiPath1 (cdr (reverse emission)) switchPr s1 s2 p1 p2) (- 1 switchPr) (- 1 p1))
                                                    (* (viterbiPath1 (cdr (reverse emission)) switchPr s2 s1 p2 p1) switchPr (- 1 p1))
                                                    (* (viterbiPath1 (cdr (reverse emission)) switchPr s1 s2 p1 p2) (- 1 switchPr) (- 1 p2))
                                                    (* (viterbiPath1 (cdr (reverse emission)) switchPr s2 s2 p2 p1) switchPr (- 1 p2)))))))

(viterbiPath '(0 0 1 1 0) 0.45 'Fair 'Biased 0.5 0.65)
(viterbiPath '(1 0 0 0 1) 0.45 'Fair 'Biased 0.5 0.65)
(viterbiPath '(1 0 1) 0.45 'Fair 'Biased 0.5 0.55)
(viterbiPath '(1 1 0 0 1) 0.45 'Fair 'Biased 0.5 0.65)

;0.0025701038085937508
;0.002102812207031251
;0.018906250000000003
;0.0016175478515625004

;(viterbiPath '(1 0 1) 0.45 'Fair 'Biased 0.5 0.55)
