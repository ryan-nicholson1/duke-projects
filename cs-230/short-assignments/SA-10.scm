;; Ryan Nicholson
;; October 18, 2017
;; SA-10: SICP 4.15

;; Assume that we have a procedure halts? which determines
;; if a procedure halts on a given object a, or if it runs forever.

(define (run-forever) (run-forever))

(define (try p)
  (if (halts? p p)
      (run-forever)
      'halted))

;; when p = try
;; (try try)
;; if (halts? try try) == #t, the result is (run-forever)
;; if (halts? try try) == #f, the result is 'halted

;; So, if (try try) halts, then (try try) will run forever.
;; While on the other hand, if (try try) runs forever, then (try try) will halt.

;; Both of these answers are paradoxical and return the opposite of what we want
;; them to be doing.

;; In conclusion, the function provides a direct contradiction,
;; and thus halts? cannot exist.