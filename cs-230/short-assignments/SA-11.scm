;;; Ryan Nicholson
;;; October 23, 2017
;;; SA-11: The Halting Problem Code Analysis

;;; PROBLEM 1
; a) 
(lambda (x) (sqrt (- 1 (* x x)))) ;; OUTPUT --> #<procedure>

; b)
'(lambda (x) (sqrt (- 1 (* x x)))) ;; OUTPUT --> (lambda (x) (sqrt (- 1 (* x x))))

; c)
(quote (lambda (x) (sqrt (- 1 (* x x))))) ;; OUTPUT --> (lambda (x) (sqrt (- 1 (* x x)))) 

; d)
(eval '(lambda (x) (sqrt (- 1 (* x x))))) ;; OUTPUT --> #<procedure:...swindle/base.rkt:496:11>

; e)
((lambda (x) (sqrt (- 1 (* x x)))) .5) ;; OUTPUT --> 0.8660254037844386

; f)
(apply (lambda (x) (sqrt (- 1 (* x x)))) '(.5)) ;; OUTPUT --> 0.8660254037844386

; g)
;(apply '(lambda (x) (sqrt (- 1 (* x x)))) '(.5))

; OUTPUT
;apply: contract violation
;  expected: procedure?
;  given: (lambda (x) (sqrt (- 1 (* x x))))
;  argument position: 1st
;  other arguments...:

; h)
(apply (eval '(lambda (x) (sqrt (- 1 (* x x))))) '(.5)) ; OUTPUT --> 0.8660254037844386

;;; PROBLEM 2

; My answers to 1e, 1f, and 1h were the same number. The answer to 1g gave an error message.
; When using apply in Scheme, you must be applying an input on a procedure. Thus, we got
; and error message due to the fact that '(lambda (x) (sqrt(- 1 (* x x)))) is not a procedure.
; The output of '(lambda (x) (sqrt(- 1 (* x x)))) just prints the statement without the quote.
; What differentiates 1e, 1f, and 1h from 1g is the procedures being "applied".

;;; Problem 3

(define (safe-code? (eval text) text arg))
; Assume that (eval text) results in the value prog.

; EX
; (safe-code? (lambda (x) (sqrt (- 1 (* x x))))
;             '(lambda (x) (sqrt (- 1 (* x x))))
;             x)

; BASE CASE: x=0
; [proc (safe-code? [proc (sqrt (- 1 (* x x)))] '(proc (x) (sqrt (- 1 (* x x)))) [x=0])]
; ... (eval (sqrt (- 1 (* x x))) [x=0]) ...
; ... [(sqrt (- 1 (* 0 0)))] ...
; ... [1] ...
; [proc (safe-code? [1] '(lambda (x) (sqrt (- 1 (* x x)))) [x=0])]
; ... (if (equals? [1] (apply (eval '(proc (x) (sqrt (- 1 (* x x)))) '(x=0))))
; ... (if (equals? [1] [1]))
; #t