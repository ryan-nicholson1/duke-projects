;; Ryan Nicholson ;;
;; SA-6 ;;

;; Normal Recursive Function ;;

(define (reverse lst)
  (if (null? lst)
     '()
     (append (reverse (cdr lst)) (list (car lst)))))

;(reverse '())
;(reverse '(1))
;(reverse '(1 2 3 4 5))
;(reverse '(1 (2 3) 4 5))

;; OUTPUT ;;

;()
;(1)
;(5 4 3 2 1)
;(5 4 (2 3) 1)

;; Tail Recursive Function ;;

(define (reverse2 lst)
  (define (result lst reslst)
    (if (null? lst)
        reslst
        (result (cdr lst) (cons (car lst) reslst))))
  (result lst '()))

;(reverse2 '())
;(reverse2 '(1))
;(reverse2 '(1 2 3 4 5))
;(reverse2 '(1 (2 3) 4 5))

;; OUTPUT ;;

;()
;(1)
;(5 4 3 2 1)
;(5 4 (2 3) 1)