;; Ryan Nicholson
;; September 22, 2016
;; SA-7

(define (deep-reverse lst)
  (if (null? lst)
      lst
      (if (list? (car lst))
          (append (deep-reverse (cdr lst)) (cons (deep-reverse (car lst)) '()))
          (append (deep-reverse (cdr lst)) (list (car lst))))))

;; Output
(reverse '())
(deep-reverse '())
(reverse '(1 (2 3) 4 5))
(deep-reverse '(1 (2 3) 4 5))
(reverse '(1 (2 3) (4 (5 6) 7) (8 9 (10 11 (12)))))
(deep-reverse '(1 (2 3) (4 (5 6) 7) (8 9 (10 11 (12)))))

;()
;()
;(5 4 (2 3) 1)
;(5 4 (3 2) 1)
;((8 9 (10 11 (12))) (4 (5 6) 7) (2 3) 1)
;((((12) 11 10) 9 8) (7 (6 5) 4) (3 2) 1)

;; What is the measure of complexity?

;; The measure of complexity is determined by the amount of sublists within the
;; main list. The higher the order of sublists (a sublist within a sublist)
;; makes the procedure more clomplex.

;; What is the order of growth of the running time of deep-reverse?

;; O(deep-reverse '()) = c
;; O(deep-reverse '(1 (2 3) 4 5) = c + n + (n^2)
;; O(deep-reverse '(1 (2 3) (4 (5 6) 7) (8 9 (10 11 (12))))) = c + n + (n^2) + (n^2) + (n^3) + (n^2) + (n^3) + (n^4)
;; ... = c + n + 3(n^2) + 2(n^3) + (n^4)
;; ... = n + (n^k) , where k is the highest order of sublists